## Hi, I'm Jonathan, nice to meet you! <img src="https://user-images.githubusercontent.com/1303154/88677602-1635ba80-d120-11ea-84d8-d263ba5fc3c0.gif" width="28px" alt="hi"> 

### Take a seat :)
![visitors](https://visitor-badge.glitch.me/badge?page_id=Baronhez.Baronhez)

I'm Jonathan Ródenas López, a DevOps student and FrontEnd Developer. I spend most of time learning about cloud technology.
#### I have a website, check it out!
[![Website Badge](https://img.shields.io/website-up-down-green-red/http/monip.org.svg)](https://jonthan.xyz/)

:mailbox: Reach me out! 

[![Linkedin Badge](https://img.shields.io/badge/-Jonathan-0e76a8?style=flat&labelColor=0e76a8&logo=linkedin&logoColor=white)](https://es.linkedin.com/in/jonathan-r%C3%B3denas-l%C3%B3pez-45400115a)  [![Mail Badge](https://img.shields.io/badge/-jonathanrodenaslopez1-c0392b?style=flat&labelColor=c0392b&logo=gmail&logoColor=white)](mailto:jonathanrodenaslopez1@gmail.com)


- 🔭 I’m currently working on bemyvega
- 🌱 I’m currently learning Services Deployment
- 🤔 I’m looking to learn new ways to improve myself.
- 📫 How to reach me: jonathanrodenaslopez1@gmail.com
- 😄 Pronouns: Jonathan, Jony
- ⚡ Fun fact: I love to collect and build mechanical keyboards. I also love playing videogames and doing graphical compositions using Photoshop.

![Baronhez's github stats](https://github-readme-stats.vercel.app/api?username=Baronhez&count_private=true&custom_title=Jonathan%20Github%20Stats&theme=dracula&&hide_border=True&hide=contribs,prs) ![Baronhez's most used Languages stats](https://github-readme-stats.vercel.app/api/top-langs/?username=Baronhez&&layout=compact&hide_border=True&exclude_repo=My_Arch_Dotfiles&theme=dracula)


### Top Technologies

![Docker](https://img.shields.io/badge/docker-%230db7ed.svg?style=for-the-badge&logo=docker&logoColor=white) ![Kubernetes](https://img.shields.io/badge/kubernetes-%23326ce5.svg?style=for-the-badge&logo=kubernetes&logoColor=white) ![Terraform](https://img.shields.io/badge/terraform-%235835CC.svg?style=for-the-badge&logo=terraform&logoColor=white)


<details>
<summary>
  More stuff about me
</summary>

### Btw I use
[![Arch Badge](https://img.shields.io/badge/Arch_Linux-1793D1?style=for-the-badge&logo=arch-linux&logoColor=white)](https://wiki.archlinux.org/)
</details>
